PROJECT( kmessbot_echo )

#### Main switches ####

# Define the default build type
# Possible values:
# - none
# - release
# - debug
# - debugfull       (even fewer optimisations)
# - relwithdebinfo  (release with debug info)
# - minsizerel      (minimum size release)
# Uncomment the next line to force building in full debug mode
SET( CMAKE_BUILD_TYPE debugfull )

#### System checks ####

# - To add checks, see in one or more of these paths
#   /usr/share/cmake/Modules
#   /usr/share/kde4/apps/cmake/modules
#   /usr/share/apps/cmake
# To add custom checks, add .cmake files into the /cmake/ dir and FIND_PACKAGE it

# Add the /cmake/ dir to the CMake module paths
SET( CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake/" ${CMAKE_MODULE_PATH} )


# Check for cmake, same as minimum for KDE 4.0.0
CMAKE_MINIMUM_REQUIRED( VERSION 2.4.5 )

FIND_PACKAGE( Qt4 REQUIRED )
FIND_PACKAGE( LibKMess-Msn REQUIRED )
#FIND_PACKAGE( LibXml2 )
#FIND_PACKAGE( LibXslt )

ADD_DEFINITIONS( ${QT_DEFINITIONS} )
INCLUDE_DIRECTORIES( ${QT_INCLUDES} ) #${LIBXML2_INCLUDE_DIR} )


#### Define the app version number ####

# Switch between the following two commands to force using a predefined version number
#EXECUTE_PROCESS( COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/cmake/get-svn-version.sh OUTPUT_VARIABLE KMESS_VERSION )
# SET( KMESS_VERSION "2.1dev" )


#### Define compiler flags ####

# Tune the debug build with even more flags
# also see FindKDE4Internal.cmake for more defaults
IF( CMAKE_COMPILER_IS_GNUCXX )
  # skipped: -ansi -pendantic -Wfatal-errors -Wold-style-cast -Wconversion
  SET( CMAKE_CXX_FLAGS_DEBUG     "-O0 -g -Wall" )
  SET( CMAKE_CXX_FLAGS_DEBUGFULL "-O0 -g3 -fno-inline -Wall -Woverloaded-virtual -Wsign-compare -Wundef" )
  # the -fvisibility option makes symbols also visible to the KDE backtrace dumper, but
  # the MinGW gcc doesn't seem to understand it well, so only add it if we're not on win32
  IF( NOT WIN32 )
    SET( CMAKE_CXX_FLAGS_DEBUGFULL "${CMAKE_CXX_FLAGS_DEBUGFULL} -fvisibility=default" )
  ENDIF( NOT WIN32 )
ENDIF( CMAKE_COMPILER_IS_GNUCXX )


# Change symbol visibility, to obtain clearer KDE backtraces
SET( __KDE_HAVE_GCC_VISIBILITY 0 )


# Set a default installation path
IF( NOT CMAKE_INSTALL_PREFIX )
  SET( CMAKE_INSTALL_PREFIX "/usr" )
ENDIF( NOT CMAKE_INSTALL_PREFIX )

#### Other settings ####

# Generate config-kmess.h
#CONFIGURE_FILE( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/config-kmessbot_echo.h.in ${CMAKE_CURRENT_BINARY_DIR}/config-kmessbot_echo.h )
#INCLUDE_DIRECTORIES( ${CMAKE_CURRENT_BINARY_DIR} )

# continue in directories
#ADD_SUBDIRECTORY( data )
#ADD_SUBDIRECTORY( doc )
ADD_SUBDIRECTORY( src )
