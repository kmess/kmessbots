
INCLUDE_DIRECTORIES(
  ${QT_INCLUDES}
  ${LIBKMESS_MSN_INCLUDE_DIR} )

########### next target ###############

SET(kmessbot_echo_SOURCES
   main.cpp
   echobot.cpp
)

SET(kmessbot_echo_LIBS
  #   ${LIBXML2_LIBRARIES}
  #   ${LIBXSLT_LIBRARIES}
   ${QT_LIBRARIES}
   ${QTXML_LIBRARIES}
   ${QT_QTTEST_LIBRARY}
   ${QT_QTCORE_LIBRARY}
   ${LIBKMESS_MSN_LIBRARY}
)

# Run Qt's moc on the headers of QObject-derived classed
QT4_WRAP_CPP( kmessbot_echo_MOC
              echobot.h
            )

ADD_EXECUTABLE( kmessbot_echo
                ${kmessbot_echo_MOC}
                ${kmessbot_echo_SOURCES}
              )
TARGET_LINK_LIBRARIES( kmessbot_echo ${kmessbot_echo_LIBS} )

#INSTALL( TARGETS kmessbot_echo DESTINATION ${BIN_INSTALL_DIR} )
