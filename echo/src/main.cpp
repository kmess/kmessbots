
#include <QtCore/QCoreApplication>
#include <QtCore/QSettings>

#include <KMess/NetworkGlobals>

#include "echobot.h"

int main( int argc, char *argv[] )
{
  QCoreApplication app( argc, argv );

  QSettings settings( "echobot.conf", QSettings::IniFormat );

  // You will be able to set your own handle in echobot.conf.
  QString handle = settings.value( "handle", "echobot@kmess.org" ).toString();
  QString password = settings.value( "password", "" ).toString();
  KMess::MsnStatus status = (KMess::MsnStatus)settings.value( "initialstatus", KMess::OnlineStatus ).toInt();
  bool reconnect = settings.value( "reconnect", "1" ).toString() != "0";

  EchoBot echoBot( handle, password );
  echoBot.setReconnect( reconnect );
  echoBot.startLogin( status );

  return app.exec();
}
