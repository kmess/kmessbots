
#ifndef ECHOBOT_H
#define ECHOBOT_H

#include <QtCore/QObject>
#include <QtCore/QTimer>

#include <KMess/NetworkGlobals>
#include <KMess/Message>

namespace KMess
{
  class MsnChat;
  class MsnSession;
  class Message;
  class MsnContact;
}

class EchoBot : public QObject
{
  Q_OBJECT

  public:
          EchoBot( const QString &handle, const QString &password );
    void  startLogin( KMess::MsnStatus status );
         ~EchoBot();
    void  setReconnect( bool r );

  private slots:
    void  connecting();
    void  loggedIn();
    void  loggedOut(KMess::DisconnectReason);
    void  chatCreated( KMess::MsnChat* );
    void  quit();
    void  reconnect();

    void  chatSendingFailed( KMess::Message message, const QString &recipient );
    void  chatSendingSucceeded( KMess::Message message );
    void  chatContactJoined( KMess::MsnContact *contact );
    void  chatContactLeft( KMess::MsnContact *contact, bool isChatIdle );
    void  chatContactTyping( KMess::MsnContact *contact );
    void  chatMessageReceived( KMess::Message message );
    void  statusEvent( const KMess::StatusMessageType,
                       const KMess::StatusMessage, const QVariant& );

  private:
    QString handle_;
    QString password_;
    KMess::MsnStatus status_;
    KMess::MsnSession *session_;
    QTimer  timer_;
    bool    reconnect_;
};

#endif
