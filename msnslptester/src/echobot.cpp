
#include <QtCore/QDebug>
#include <QtCore/QCoreApplication>

#include <KMess/Message>
#include <KMess/TextMessage>
#include <KMess/MsnChat>
#include <KMess/MsnContact>
#include <KMess/MsnSession>
#include <KMess/NetworkGlobals>

#include "echobot.h"

EchoBot::EchoBot( const QString &handle, const QString &password )
: QObject()
, handle_( handle )
, password_( password )
, status_( KMess::OnlineStatus )
, session_( 0 )
, reconnect_( true )
{
  qDebug() << "***** Creating... *****";

  session_ = new KMess::MsnSession();

  connect( session_, SIGNAL(  connecting() ),
           this,     SLOT(    connecting() ) );
  connect( session_, SIGNAL(    loggedIn() ),
           this,     SLOT(      loggedIn() ) );
  connect( session_, SIGNAL(   loggedOut(KMess::DisconnectReason) ),
           this,     SLOT(     loggedOut(KMess::DisconnectReason) ) );
  connect( session_, SIGNAL( chatCreated( KMess::MsnChat* ) ),
           this,     SLOT(   chatCreated( KMess::MsnChat* ) ) );
  connect( session_, SIGNAL( statusEvent( const KMess::StatusMessageType,
                                          const KMess::StatusMessage,
                                          const QVariant& ) ),
           this,     SLOT(   statusEvent( const KMess::StatusMessageType,
                                          const KMess::StatusMessage,
                                          const QVariant& ) ) );

  connect( &timer_,  SIGNAL(     timeout() ),
           this,     SLOT(          quit() ) );

  //session_->setSessionSetting( "NotificationServerOverride", "127.0.0.1" );
  //session_->setSessionSetting( "SoapRedirectionServer",      "127.0.0.1" );

  timer_.setSingleShot( true );

  qDebug() << "***** Created! *****";
}

EchoBot::~EchoBot()
{
  qDebug() << "***** Destroying... *****";
  delete session_;

  qDebug() << "***** Destroyed! ******";
}

void EchoBot::quit()
{
  QCoreApplication::quit();
}

void EchoBot::chatContactJoined( KMess::MsnContact *contact )
{
  qDebug() << "***** Contact " << contact << " joined the chat! *****";
}

void EchoBot::chatContactLeft( KMess::MsnContact *contact, bool isChatIdle )
{
  qDebug() << "***** Contact " << contact << " left the chat! (chatIdle="
           << isChatIdle << ") *****";
}

void EchoBot::chatContactTyping( KMess::MsnContact *contact )
{
  qDebug() << "***** Contact " << contact << " is typing... *****";
}

void EchoBot::chatCreated( KMess::MsnChat *chat )
{
  qDebug() << "***** A new chat was created: " << chat << " *****";
 
  connect( chat, SIGNAL(     sendingFailed( KMess::Message, const QString& )),
           this, SLOT(   chatSendingFailed( KMess::Message, const QString& )));
  connect( chat, SIGNAL(  sendingSucceeded( KMess::Message ) ),
           this, SLOT(chatSendingSucceeded( KMess::Message ) ) );
  connect( chat, SIGNAL(     contactJoined( KMess::MsnContact* ) ),
           this, SLOT(   chatContactJoined( KMess::MsnContact* ) ) );
  connect( chat, SIGNAL(       contactLeft( KMess::MsnContact*, bool ) ),
           this, SLOT(     chatContactLeft( KMess::MsnContact*, bool ) ) );
  connect( chat, SIGNAL(     contactTyping( KMess::MsnContact* ) ),
           this, SLOT(   chatContactTyping( KMess::MsnContact* ) ) );
  connect( chat, SIGNAL(   messageReceived( KMess::Message ) ),
           this, SLOT( chatMessageReceived( KMess::Message ) ) );
}

void EchoBot::chatSendingFailed( KMess::Message message, const QString &recipient )
{
  qDebug() << "***** Failed to send message. *****";
}

void EchoBot::chatSendingSucceeded( KMess::Message message )
{
  qDebug() << "***** Message send succesful. *****";
}

void EchoBot::chatMessageReceived( KMess::Message message )
{
  Q_ASSERT( message.chat() == sender() );
  Q_ASSERT( message.isValid() );

  if( message.type() == KMess::InvalidMessageType )
  {
    const QString &mimeType( message.field( "Content-Type", QString() ) );
    if( mimeType.isEmpty() )
    {
      qDebug() << "*** Empty message type received! ***";
    } else if( mimeType == "text/x-msmsgsinvite" ) {
      qDebug() << "*** Messenger invite received! ***";
    } else {
      qDebug() << "*** Unknown message type received: " << mimeType << "***";
    }
    return;
  }

  if( message.type() != KMess::TextMessageType )
  {
    qDebug() << "***** Unknown message type " << message.type() << " received. *****";
    return;
  }

  qDebug() << "***** Chat message received. Echoing! *****";

  KMess::TextMessage textmessage( message );
  Q_ASSERT( textmessage.isValid() );

  if( textmessage.message().toLower() == "die, echobot!" )
  {
    qWarning() << "***** Peer " << textmessage.peer() << " is telling us to die! *****";
    QCoreApplication::quit();
    return;
  }

  KMess::TextMessage echo( textmessage.chat(), textmessage.peer() );
  echo.setColor( textmessage.color() );
  echo.setEmoticons( textmessage.emoticons() );
  echo.setFont( textmessage.font() );
  echo.setMessage( textmessage.message() );

  qDebug() << textmessage.message();

  echo.chat()->sendMessage( echo );

  qDebug() << "***** Echoed! *****";
}

void EchoBot::connecting()
{
  qDebug() << "***** Echobot is connecting! *****";
}

void EchoBot::loggedOut(KMess::DisconnectReason r)
{
  qDebug() << "***** Echobot logged out! Reason=" << r << " *****";
  if( reconnect_ )
  {
    QTimer::singleShot( 1000, this, SLOT( reconnect() ) );
  }
  else
  {
    QCoreApplication::exit(1);
  }
}

void EchoBot::loggedIn()
{
  qDebug() << "***** Echobot logged in! *****";
  //timer_.start( 60000 );
}

void EchoBot::reconnect()
{
  startLogin( status_ );
}

void EchoBot::setReconnect( bool r )
{
  reconnect_ = r;
}

void EchoBot::startLogin( KMess::MsnStatus status )
{
  status_ = status;
  session_->logIn( handle_, password_, status_ );
}

void EchoBot::statusEvent( const KMess::StatusMessageType type,
                           const KMess::StatusMessage m, const QVariant &i )
{
  qDebug() << "*** Status event from library; type=" << type
           << "; message=" << m << "; variant=" << i;

  if( type == KMess::ErrorMessage && ( m == 13 || m == 17 ) )
  {
    // Reconnect!
    loggedOut(KMess::UnexpectedDisconnect);
  }
}
